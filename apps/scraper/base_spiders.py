# -*- coding: utf-8 -*-
# Copyright (C) Leadspicker s.r.o. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Vojtěch Oram <vojtech@Leadspicker.com>, 2018
import json
import scrapy
import logging
import itertools
import urllib.request, urllib.parse, urllib.error

from apps.scraper.parser import ResponseParser, make_full_url

logger = logging.getLogger('crawlers')


class ProfileListSpider(scrapy.Spider):
    """
    Basic spider
    1. Scrape links to startup profiles and make requests from them
    2. Make request for another page if  there is any
    3. Process profile pages
    Loop until there are no more links to profiles
    """
    # delay between requests in seconds
    download_delay = 2

    # in following statements, "selector" means tuple (selector type, string), for example (By.CSS, "a.name::attr(href)")

    # selector of links to startup detail page
    list_selector = None
    profile_selector = None
    # selector of links to next pages of startups list (can be more than one)
    next_selector = None
    # selector of link to next page (if there is only one)
    single_next_selector = None
    # URL template for next page, for example https://examples.com/startups?page={page}
    # You can use variable {page} for 1-indexed pages and {page0} for 0-indexed pages
    next_page_template = None
    max_page = None
    # dictionary of profile fields
    # Key is field name, value is selector
    profile_selectors = {}
    # Which fields have to be filled in results so it can be passed to pipelines
    required_fields = ['name', 'url']
    profile_headers = {}
    profile_cookies = {}
    ignored_data = {}
    use_formrequest_for_pages = False
    report_defunct_spider = True
    monitoring_delay_threshold = None

    def parse(self, response):
        parser = ResponseParser(response)
        url_count = 0
        if self.list_selector:
            for url in parser.find_elements(*self.list_selector):
                url = make_full_url(response, url)
                r = response.request.replace(
                    url=url,
                    callback=self.parse_profile,
                    meta=dict(
                        detail=url,
                    )
                )
                if self.profile_headers:
                    r = r.replace(headers=self.profile_headers)
                if self.profile_cookies:
                    r = r.replace(cookies=self.profile_cookies)
                url_count += 1
                yield r
        elif self.profile_selector:
            for selector in parser.find_element(*self.profile_selector, return_selector=True):
                url_count += 1
                for d in self.parse_profile(selector, meta=response.meta):
                    yield d

        if not url_count:
            # stop spider
            raise scrapy.exceptions.CloseSpider("No items on page --> close spider")
        if self.next_selector:
            for url in parser.find_elements(*self.next_selector):
                yield response.request.replace(url=make_full_url(response, url))
        if self.single_next_selector:
            partial_url = parser.find_element(*self.single_next_selector)
            if partial_url:
                yield response.request.replace(url=make_full_url(response, partial_url))
        if self.next_page_template:
            page = response.meta.get('page', 2)
            if not self.max_page or page < self.max_page:
                yield response.request.replace(url=make_full_url(response, self.next_page_template.format(page=page, page0=page - 1)), meta=dict(page=page + 1))
        if self.use_formrequest_for_pages:
            page = response.meta.get('page', 1)
            formdata = response.meta.get('formdata', {'page': page})
            page += 1
            formdata['page'] = str(page)
            yield response.request.replace(formdata=formdata, meta={'page': page})

    def parse_profile(self, response, meta=None):
        parser = ResponseParser(response)
        data = {
            'detail': (meta if meta else response.meta).get('detail', response.url if not meta else "")
        }
        for key, selector in list(self.profile_selectors.items()):
            if isinstance(selector, list):
                data.setdefault(key, [])
                for subselector in selector:
                    val = parser.find_element(*subselector)
                    if val:
                        data[key].append(self.strip(val))
            else:
                val = parser.find_element(*selector)
                if val:
                    data[key] = self.strip(val)

        for key, val in list(self.ignored_data.items()):
            if key in data and data[key] in val:
                data = {}
                break
        # check required fields filled
        if set(self.required_fields).issubset(list(data.keys())):
            yield data
        else:
            logger.info("Scraped item thrown out, required field missing (data: %s)", data)

    def strip(self, data):
        if isinstance(data, str):
            return data.strip()
        return data

    def get_json_data(self, response):
        data = {}
        try:
            data = json.loads(response.text)
        except ValueError as e:
            raise Exception("ValueError when decoding response JSON: %s, data: %s" % (e, response.body))
        return data


class AlgoliaSpider(scrapy.Spider):
    download_delay = 0

    source_name = None
    tags = []
    specializations = []
    list_selector = None
    next_selector = None
    required_fields = ['name', 'url']
    hits_per_page = 100
    max_values_per_facet = 1000
    cdn = "dsn"
    report_defunct_spider = True
    monitoring_delay_threshold = None
    interval = 'weekly'

    def __init__(self, tags=None, specializations=None, *args, **kwargs):
        super(AlgoliaSpider, self).__init__(*args, **kwargs)
        assert self.source_name, "Fill attribute source_name!"

        assert self.algolia_id, "Fill attribute algolia_id!"
        assert self.algolia_key, "Fill attribute algolia_key!"
        assert self.algolia_agent, "Fill attribute algolia_agent!"
        assert self.index_name, "Fill attribute index_name!"
        assert self.referer, "Fill attribute referer!"

        assert self.parse_profile, "Define parse_profile(hit) method"

    def start_requests(self):
        yield self.get_request(page=0)

    def url_encode(self, text):
        return urllib.parse.urlencode({"a": text})[2:]

    def get_request(self, page=1, facets=None):
        meta_dict = {"page": page}

        facet_string = ""
        if facets:
            meta_dict['facets'] = facets
            facet_string = "&hitsPerPage={}&maxValuesPerFacet={}&facetFilters=".format(
                self.hits_per_page,
                self.max_values_per_facet,
                self.index_name,
            )
            facet = []
            for key, val in list(facets.items()):
                facet.append(["%s:%s" % (key, val)])
            facet_string += urllib.parse.quote(json.dumps(facet))

        body = '{"requests":[{"indexName":"' + self.index_name + '","params":"query=&page=' + str(page) + facet_string + '"}]}'
        return scrapy.FormRequest(
            "https://%s-%s.algolia.net/1/indexes/*/queries?x-algolia-agent=%s&x-algolia-application-id=%s&x-algolia-api-key=%s" % (
                self.algolia_id.lower(), self.cdn, self.algolia_agent, self.algolia_id, self.algolia_key),
            method="POST",
            body=body,
            headers={"Referer": self.referer},
            meta=meta_dict
        )

    def parse(self, response):
        page = response.meta.get('page', 1)
        facets = response.meta.get('facets', None)

        results = self.get_json_data(response)["results"][0]
        hits = results["hits"]
        pages = results["nbPages"]
        # Parse
        for out in map(self.parse_profile, hits):
            if set(self.required_fields).issubset(list(out.keys())):
                yield out
        if pages > page:
            yield self.get_request(page + 1, facets=facets)

    def get_json_data(self, response):
        try:
            data = json.loads(response.text)
        except ValueError as e:
            raise Exception("ValueError when decoding response JSON: %s, data: %s" % (e, response.text))
        return data
