# -*- coding: utf-8 -*-
# Django modules
from django.core.validators import URLValidator, validate_email as EmailValidator
from django.core.exceptions import ValidationError
from django.utils.encoding import force_text

# Standard modules
import re
import logging
import json
import random
import string
import urllib.parse
from functools import reduce


logger = logging.getLogger(__name__)


def repair_json(data):
    try:
        json.loads(data)
    except ValueError:
        # Trying replace bad quotations with good ones
        data = data.replace("”", "\"").replace("“", "\"")
        # Replacing newlines with \n
        data = data.replace("\r\n", " " * 400).replace("\n", " " * 400)
        data = json.dumps(json.loads(data), indent=4).replace(" " * 400, "\\n")
    return data


def repair_total_funding(funding):

    # Check if there is something to repair
    if not funding:
        return None

    # Check if it is not already a number
    if isinstance(funding, (int, float)):
        return funding

    funding = funding.strip()
    from libs.data import EXCHANGE_RATES, CURRENCY_DICT
    re_cur = "|".join(["|".join(x).replace("[", "(?:").replace("]", ")?").replace(".", r"\.").replace("$", r"\$") for x in iter(CURRENCY_DICT.items())])
    m = re.match(r"^(" + re_cur + r")?\s*([\d.,]+)\s*(?:([MKB])|(" + re_cur + r")|([MKB])\s*(" + re_cur + r")?)?$", funding, flags=re.U | re.DOTALL | re.I)
    if not m:
        return None
    cur1, number, coef1, cur2, coef2, cur3 = m.groups()
    currency = cur1 or cur2 or cur3
    coef = coef1 or coef2

    m = re.match(r"^(?:(\d+)|([\d.]+)|([\d,]+)|([\d,]+)\.\d+|([\d.]+),\d+)$", number)
    if not m:
        return None
    value = int(reduce(lambda x, y: x or y, m.groups()).replace(",", "").replace(".", "")) * ({"K": 1000, "M": 10**6, "B": 10**9}.get((coef or "").upper(), 1))

    if not currency:
        return value

    # Get currency and exchange rate, convert and return
    exchange_rate = 1
    if currency.lower() in CURRENCY_DICT:
        c = CURRENCY_DICT.get(currency.lower(), "")
        if c != "USD":
            exchange_rate = EXCHANGE_RATES.get("USD" + c, None)
            if not exchange_rate:
                return None
        return int(value / exchange_rate) if value else None
    else:
        return None


ipv4_address = re.compile(r'^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')
ipv6_address = re.compile(
    r'^(?:(?:[0-9A-Fa-f]{1,4}:){6}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|::(?:[0-9A-Fa-f]{1,4}:){5}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,4}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){,6}[0-9A-Fa-f]{1,4})?::)$')


def is_ip(string):
    return re.match(ipv4_address, string) or re.match(ipv6_address, string)


invalid_third_level_domain = re.compile(r"^(?:www\d|.{1,3})$")


def get_domain(url, name_only=False, ignore_email_providers=False):
    if not url:
        return ''
    from .data import freemail_domains
    # fix empty space in input text
    url = url.split()[0] if url.split() else ''
    if '.' not in url:
        return ''
    try:
        if not url.startswith('http'):
            url = 'http://' + url
        parsed_uri = urllib.parse.urlparse(url)
        domain = '{uri.netloc}'.format(uri=parsed_uri)
        domain = domain.split(":")[0]
        if domain.endswith("."):
            domain = domain[:-1]
        domain = re.sub(r'[^\w\d.-]+', '', domain, flags=re.U)
        if is_ip(domain):
            return domain
        if ignore_email_providers and domain in freemail_domains:
            return ""
        if domain.startswith("www.") or (domain.count(".") >= 2 and len(domain.split(".")[1]) > 2 and invalid_third_level_domain.match(domain.split(".")[0])):
            domain = domain.split(".", 1)[1]
        if name_only:
            if domain.count(".") >= 2 and len(domain.split(".")[-2]) > 3:
                domain = domain.split(".")[-2]
            else:
                domain = domain.split(".")[0]
    except BaseException:
        domain = ""
    if len(domain) > 200:
        if len(domain.split('.')) > 2:
            domain = '.'.join(domain.split('.')[-2:])
        else:
            domain = domain[len(domain) - 200:]
    return domain.lower()


def get_domain_and_subdomain_list(url, ignore_email_providers=False):
    # special cases for making domain from subdomain, like gitoff.zendesk.com --> gitoff.com
    domain_from_subdomains = [
        'zendesk.com',
    ]
    out = []
    for u in re.split(r"[\s,;]", url):

        domain = get_domain(u, ignore_email_providers=ignore_email_providers)
        parts = domain.split('.')
        if len(parts) > 2:
            if '.'.join(parts[-2:]) in domain_from_subdomains:
                out.extend([domain, '.'.join(parts[:-2] + [parts[-1]])])
            else:
                out.extend([domain, '.'.join(parts[-2:])])
        elif len(parts) > 1:
            out.append(domain)
    return out


def repairurl(url):
    if not isinstance(url, str):
        return url
    """
        Some urls in source DBs are broken. This function repairs part of them
    """
    try:
        if re.match(r"\.?[^.]+\.[^.]{1,7}", url) and not url.startswith("http://") and not url.startswith("https://"):
            return "http://" + url if url[0] != "." else url[1:]
        if re.match(r"http://\.[^.]+\.[^.]{1,7}", url):
            return "http://www" + url[7:]
        if re.search("href.?=", url):
            return re.sub(".*href.?=.?[\"'](.*?)[\"'].*", "\\1", url)
    except BaseException:
        logger.info("Unable to repair url: %s" % repr(url))
        return url
    return url


def validate_url(url):
    validate = URLValidator()
    try:
        validate(url)
        return True
    except ValidationError:
        return False


def validate_email(email):
    try:
        EmailValidator(email)
        return True
    except ValidationError:
        return False


def stripify(variable):
    typ = type(variable)
    if typ == str:
        return variable.strip()
    if typ == bytes:
        return unicodize(variable).strip()
    elif typ == list:
        return list(map(stripify, variable))
    elif typ == tuple:
        return tuple(map(stripify, variable))
    elif typ == dict:
        return dict(map(stripify, iter(variable.items())))
    else:
        return variable


def utifize(variable):
    typ = type(variable)
    if typ == str:
        return variable.encode("utf8", errors="ignore").replace(b'\x00', b'')
    elif typ == list:
        return list(map(utifize, variable))
    elif typ == set:
        return set(map(utifize, variable))
    elif typ == tuple:
        return tuple(map(utifize, variable))
    elif typ == dict:
        return dict(map(utifize, iter(variable.items())))
    else:
        return variable


def sanitize(variable):
    typ = type(variable)
    if typ == str:
        return variable.replace("<", "&lt;").replace(">", "&gt;")
    elif typ == bytes:
        return unicodize(variable).replace("<", "&lt;").replace(">", "&gt;")
    elif typ == list:
        return list(map(sanitize, variable))
    elif typ == set:
        return set(map(sanitize, variable))
    elif typ == tuple:
        return tuple(map(sanitize, variable))
    elif typ == dict:
        return dict(map(sanitize, iter(variable.items())))
    else:
        return variable


def unicodize(variable):
    typ = type(variable)
    if typ == bytes:
        return force_text(variable, "utf8", errors="ignore").replace("\xea", "'").replace('\x00', '')
    elif typ == list:
        return list(map(unicodize, variable))
    elif typ == tuple:
        return tuple(map(unicodize, variable))
    elif typ == dict:
        return dict(map(unicodize, iter(variable.items())))
    else:
        return variable


def smart_truncate(content, length=100, suffix='...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-1]) + suffix


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


