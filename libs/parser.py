# -*- coding: utf-8 -*-
# Copyright (C) Leadspicker s.r.o. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Vojtěch Oram <vojtech@Leadspicker.com>, 2018
import re
from dateutil import parser
import datetime
import json
from libs import parsers


def parse_year(text):
    if not isinstance(text, str):
        return None
    res = re.findall(r'\b\d{4}\b', text)
    if res:
        return int(res[0])
    return None


def parse_min_max_employees(text):
    if isinstance(text, int):
        return text, text
    try:
        sizes = tuple(sorted(map(int, (re.findall("\d+", text) * 2)[:2])))
        return sizes if sizes else (None, None)
    except ValueError:
        return None, None


def parse_date(text):
    text = str(text).strip()
    try:
        date = parser.parse(text, default=datetime.datetime(1970, 1, 1))
    except ValueError:
        return None
    if date.year > 1970:
        return date
    return None


def parse_funding(text):
    return parsers.parse_funding(text)


def parse_redirect_link(param="url", fce=None):
    def parse(response, text):
        if not text:
            return ""

        m = re.search(param+"=([^&]*)", fce(response, text) if fce else text, flags=re.I | re.M | re.DOTALL)
        if m:
            gs = [_f for _f in m.groups() if _f]
            if gs:
                return gs[0].strip().replace("%3A", ":").replace("%2F", "/")
            return ""
        return ""
    return parse


def parse_fb_info(text):
    data = {}

    ms = re.findall("<a href=\"mailto:([^\"?]+&#064;[^\"?]+)", text, flags=re.I | re.M | re.DOTALL)  # email
    email = ms[0].replace("&#064;", "@") if ms else ""

    ms = re.findall("<img class=\"scaledImageFitWidth img\" src=\"(.*?)\" alt=\"\"", text, flags=re.I | re.M | re.DOTALL)  # logo
    logo = ms[0].replace("&amp;", "&") if ms else ""

    ms = re.findall("<div class=\"_4bl9\"><div class=\"_50f4\">[\w\s]*?([\+\-\d\s]+)</div>", text, flags=re.I | re.M | re.DOTALL)  # telephone
    telephone = ms[0] if ms else ""

    ms = re.findall("<script type=\"application/ld[+]json\">(.*?)</script>", text, flags=re.I | re.M | re.DOTALL)  # URL
    data = json.loads(ms[0]) if ms else {}

    ms = re.findall("<div class=\"_3-8w\">([^<]*?)</div></div>", text, flags=re.I | re.M | re.DOTALL)  # URL
    oneliner = ms[0] if ms else ""

    ms = re.findall("data-lynx-mode=\"[^\"]+\" id=\"[^\"]+\"><div class=\"_50f4\">([^<]*?)</div></a>", text, flags=re.I | re.M | re.DOTALL)  # URL
    url = ms[0] if ms else ""

    name = data.get("name", "")
    city = data.get("address", {}).get("addressLocality", "")
    state = data.get("address", {}).get("addressRegion", "")

    return {
        "telephone": telephone,
        "email": email,
        "url": url,
        "logo": logo,
        "oneliner": oneliner,
        "name": name,
        "city": city,
        "state": state,
    }


def parse_regexp(regexp, flags=re.I | re.DOTALL | re.M | re.U, fce=None, fce_after=None):
    def parse(response, text):
        result = ""
        if text:
            m = re.search(regexp, fce(response, text) if fce else text, flags=flags)
            if m:
                gs = [_f for _f in m.groups() if _f]
                if gs:
                    result = gs[0].strip()
        return fce_after(response, result) if fce_after else result
    return parse


def sub_regexp(regexp, repl="", flags=re.I | re.DOTALL | re.M, fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub(regexp, repl, fce(response, text) if fce else text, flags=flags).strip()
    return parse


def dehtml(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("<[^>]+?>", "", fce(response, text) if fce else text)
    return parse


def remove_whitespace(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("\s+", " ", fce(response, text) if fce else text).strip()
    return parse


def clean_query_string(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("[?][^?]+", "", fce(response, text) if fce else text)
    return parse


def add_scheme(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("^//", "https://", fce(response, text) if fce else text)
    return parse
