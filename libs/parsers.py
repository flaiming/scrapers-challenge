import re
from dateutil import parser
from functools import reduce
import datetime
import json


def parse_year(text):
    if not isinstance(text, str):
        return None
    res = re.findall(r'\b\d{4}\b', text)
    if res:
        return int(res[0])
    return None


def parse_min_max_employees(text):
    if isinstance(text, int):
        return text, text
    try:
        sizes = tuple(sorted(map(int, (re.findall("\d+", text) * 2)[:2])))
        return sizes if sizes else (None, None)
    except ValueError:
        return None, None


def parse_date(text):
    text = str(text).strip()
    try:
        date = parser.parse(text, default=datetime.datetime(1970, 1, 1))
    except ValueError:
        return None
    if date.year > 1970:
        return date
    return None


def parse_funding(funding):
    # Check if there is something to repair
    if not funding:
        return None

    # Check if it is not already a number
    if isinstance(funding, (int, float)):
        return funding
    funding = funding.strip()

    # parse funding interval
    if '-' in funding:
        fundings = [_f for _f in map(parse_funding, [x.strip() for x in funding.split('-')]) if _f]
        if len(fundings) == 2 and all(fundings):
            return (fundings[0] + fundings[1]) / 2
        elif fundings:
            return fundings[0]

    from libs.data import EXCHANGE_RATES, CURRENCY_DICT
    re_cur = "|".join(["|".join(x).replace("[", "(?:").replace("]", ")?").replace(".", "\.").replace("$", "\$") for x in iter(CURRENCY_DICT.items())])
    m = re.match("^(" + re_cur + ")?\s*([\d.,]+)\s*(?:([MKB])|(" + re_cur + ")|([MKB])\s*(" + re_cur + ")?|([MKB])\+)?$", funding, flags=re.U | re.DOTALL | re.I)
    if not m:
        return None
    cur1, number, coef1, cur2, coef2, cur3, coef3 = m.groups()
    currency = cur1 or cur2 or cur3
    coef = coef1 or coef2 or coef3
    # strip comas and dots when not decimal
    number = re.sub(r'(?:,|\.)(\d{3})', '\\1', number)

    m = re.match("^(?:(\d+)|([\d.]+)|([\d,]+)|([\d,]+)\.\d+|([\d.]+),\d+)$", number)
    if not m:
        return None
    try:
        value = int(float(reduce(lambda x, y: x or y, m.groups()).replace(",", ".")) * ({"K": 1000, "M": 10**6, "B": 10**9}.get((coef or "").upper(), 1)))
    except ValueError:
        return None

    if not currency:
        return value

    # Get currency and exchange rate, convert and return
    exchange_rate = 1
    if currency.lower() in CURRENCY_DICT:
        c = CURRENCY_DICT.get(currency.lower(), "")
        if c != "USD":
            exchange_rate = EXCHANGE_RATES.get("USD" + c, None)
            if not exchange_rate:
                return None
        return int(value / exchange_rate) if value else None
    else:
        return None


def parse_redirect_link(param="url", fce=None):
    def parse(response, text):
        if not text:
            return ""

        m = re.search(param + "=([^&]*)", fce(response, text) if fce else text, flags=re.I | re.M | re.DOTALL)
        if m:
            gs = [_f for _f in m.groups() if _f]
            if gs:
                return gs[0].strip().replace("%3A", ":").replace("%2F", "/")
            return ""
        return ""
    return parse


def parse_fb_info(text):
    data = {}

    ms = re.findall("<a href=\"mailto:([^\"?]+&#064;[^\"?]+)", text, flags=re.I | re.M | re.DOTALL)  # email
    email = ms[0].replace("&#064;", "@") if ms else ""

    ms = re.findall("<img class=\"scaledImageFitWidth img\" src=\"(.*?)\" alt=\"\"", text, flags=re.I | re.M | re.DOTALL)  # logo
    logo = ms[0].replace("&amp;", "&") if ms else ""

    ms = re.findall("<div class=\"_4bl9\"><div class=\"_50f4\">[\w\s]*?([\+\-\d\s]+)</div>", text, flags=re.I | re.M | re.DOTALL)  # telephone
    telephone = ms[0] if ms else ""

    ms = re.findall("<script type=\"application/ld[+]json\">(.*?)</script>", text, flags=re.I | re.M | re.DOTALL)  # URL
    data = json.loads(ms[0]) if ms else {}

    ms = re.findall("<div class=\"_3-8w\">([^<]*?)</div></div>", text, flags=re.I | re.M | re.DOTALL)  # URL
    oneliner = ms[0] if ms else ""

    ms = re.findall("data-lynx-mode=\"[^\"]+\" id=\"[^\"]+\"><div class=\"_50f4\">([^<]*?)</div></a>", text, flags=re.I | re.M | re.DOTALL)  # URL
    url = ms[0] if ms else ""

    name = data.get("name", "")
    city = data.get("address", {}).get("addressLocality", "")
    state = data.get("address", {}).get("addressRegion", "")

    return {
        "telephone": telephone,
        "email": email,
        "url": url,
        "logo": logo,
        "oneliner": oneliner,
        "name": name,
        "city": city,
        "state": state,
    }


def parse_regexp(regexp, flags=re.I | re.DOTALL | re.M | re.U, fce=None):
    def parse(response, text):
        if not text:
            return ""
        m = re.search(regexp, fce(response, text) if fce else text, flags=flags)
        if m:
            gs = [_f for _f in m.groups() if _f]
            if gs:
                return gs[0].strip()
            return ""
        return ""
    return parse


def sub_regexp(regexp, repl="", flags=re.I | re.DOTALL | re.M, fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub(regexp, repl, fce(response, text) if fce else text, flags=flags).strip()
    return parse


def dehtml(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("<[^>]+?>", "", fce(response, text) if fce else text)
    return parse


def remove_whitespace(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("\s+", " ", fce(response, text) if fce else text).strip()
    return parse


def clean_query_string(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("[?][^?]+", "", fce(response, text) if fce else text)
    return parse


def add_scheme(fce=None):
    def parse(response, text):
        if not text:
            return ""
        return re.sub("^//", "https://", fce(response, text) if fce else text)
    return parse
